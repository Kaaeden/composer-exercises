package com.example.composepractice

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composepractice.ui.theme.HappyBirthdayTheme

@Composable
fun BirthdayGreetingWithText(message: String, from: String)
{
    Column {
        Row {
            Text(text = message,
                fontSize = 36.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentWidth(Alignment.Start)
                    .padding(start = 16.dp, top = 16.dp)
            )
        }
        Text(text = from,
            fontSize = 20.sp,
            color = Color.Blue,
            modifier  = Modifier
                .fillMaxWidth()
                .wrapContentWidth(Alignment.CenterHorizontally)
                .padding(start = 16.dp, end = 16.dp)

        )

    }
}

@Composable
fun BirthdayCardPreview() {
    HappyBirthdayTheme {

        BirthdayGreetingWithText(message = "Happy Birthday Sam!", from ="- Abe Lincoln")
    }
}

@Composable
fun BirthdayGreetingWithImage(message: String, from: String)
{
    val image_ = painterResource(id = R.drawable.androidparty)
    Box {
        Image(
            painter = image_,
            contentDescription = null,
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth(),
            contentScale = ContentScale.Crop
        )
        BirthdayGreetingWithText(message, from)
    }
}