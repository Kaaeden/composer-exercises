package com.example.composepractice

import android.media.Image
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role.Companion.Image
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.FontWeight.Companion.Black
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composepractice.ui.theme.HappyBirthdayTheme
import org.intellij.lang.annotations.JdkConstants.HorizontalAlignment

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HappyBirthdayTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    //TaskManager()
                    //ArticlePage()
                    BusinessCard()
                    /*BirthdayGreetingWithImage(getString(R.string.happy_birthday_message), "Abe Lincoln")*/
                }
            }
        }
    }
}









@Preview(showBackground = true)
@Composable
fun BusinessCard()
{
    val image_ = painterResource(id = R.drawable.android_logo)
    Box(
        modifier = Modifier
            .background(color = Color(red = 0, green = 36, blue = 80))


    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center, modifier = Modifier
                .fillMaxHeight()
            .fillMaxWidth()) {

            Image(
                image_, contentDescription = null,
                modifier = Modifier
            )
            Text(
                "Android Developer", color = Color(red = 102, green = 138, blue = 182), modifier = Modifier
                   )
            Text("Kenneth Bolden", color = Color.White, fontSize = 16.sp, fontWeight = Bold)

        }


    }

}

